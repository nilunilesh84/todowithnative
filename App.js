import React, { useState, } from "react";
import {
  StyleSheet,
  View,
  FlatList,
  Alert,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import Todoitem from "./components/todoitem";
import Header from "./components/header";
import Addtodo from "./components/Addtodo";
import Sandbox from "./components/sandbox";


export default function App() {
  const [text, settext] = useState("");
  const [todo, setTodo] = useState([
    { text: "buy grapes", key: 1 },
    { text: "buy banana", key: 2 },
    { text: "buy mango", key: 3 },
    { text: "buy apple", key: 4 },
  ]);
  const pressHandler = (key) => {
    const data = todo.filter((item) => item.key != key);
    setTodo(data);
  };
  const addTodos = (text) => {
    settext("");
    if (text.length > 4) {
      setTodo((pre) => {
        return [{ text: text, key: Math.random().toString() }, ...pre];
      });
    } else {
      Alert.alert("OPPS", "Todos must be 4 characters long", [
        { text: "UnderStood" },
      ]);
    }
  };
  return (
    <TouchableWithoutFeedback
      onPress={() => {
        Keyboard.dismiss();
      }}
    >
      <View style={styles.container}>
        <Header />
        <Addtodo addTodos={addTodos} text={text} settext={settext} />
        <View style={styles.content}>
          <View style={styles.list}>
            <FlatList
              data={todo}
              renderItem={({ item }) => (
                <Todoitem item={item} pressHandler={pressHandler} />
              )}
            />
          </View>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: 20,
    alignItems: "center",
  },
  content: {
    flex: 1,
    marginTop: 10,
  },
  list:{
    flex:1,
    marginTop:5,
  }
});


