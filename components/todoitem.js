import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import React from "react";

import { MaterialIcons } from "@expo/vector-icons";

export default function Todoitem({ item, pressHandler }) {
  return (
    <TouchableOpacity onPress={() => pressHandler(item.key)}>
      <View style={styles.item}>
        <MaterialIcons name="delete" size={17} color={"#333"} />
        <Text>{item.text}</Text>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  item:{
    flexDirection:'row',
    justifyContent:'flex-start'
  },
  title: {
    fontSize: 20,
    textAlign: "center",
    fontWeight: "bold",
    backgroundColor: "pink",
    padding: 15,
    marginTop: 10,
    width: 330,
  },
 
});
