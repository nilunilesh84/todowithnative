import { StyleSheet, Text, View } from "react-native";
import React from "react";

export default function Header() {
  return (
    <View style={styles.header}>
      <Text style={styles.title}>My Todos</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    display:'flex',
    alignItems:'center',
    backgroundColor: "orange",
  },
  title: {
    fontSize: 20,  
    padding: 15,
    textAlign:'center',
    fontWeight: "bold",
  },
});
