import { StyleSheet, TextInput, Button, View } from "react-native";
import React from "react";

export default function Addtodo({text,settext,addTodos}) {

  const changehandle = (e) => {
    settext(e);
  };
  return (
    <View style={styles.wrapper}>
      <TextInput
        value={text}
        placeholder="enter todo"
        style={styles.input}
        onChangeText={changehandle}
      />
      <Button onPress={() => addTodos(text)} title="addtodo" />
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    // flexDirection: "row",
  },
  input: {
    borderWidth: 1,
    marginTop: 20,
    marginBottom: 10,
    paddingLeft: 10,
    borderColor: "black",
    borderRadius: 8,
    height: 38,
    width: 300,
  },
});
